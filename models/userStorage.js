const User = require("./user");

class UserStorage {
	static users = [];
	static addUser(user) {
		this.users.push(user);
	}

	static findUser({ email }) {
		return this.users.find((item) => item.email === email);
	}

	static getAllUsers() {
		return this.users;
	}
}

module.exports = UserStorage;
