const products = [
	{
		id: 1,
		name: "Product 1",
		price: 10,
		image: "https://source.unsplash.com/400x200/?City",
	},
	{
		id: 2,
		name: "Product 2",
		price: 20,
		image: "https://source.unsplash.com/400x200/?City",
	},
	{
		id: 3,
		name: "Product 3",
		price: 30,
		image: "https://source.unsplash.com/400x200/?City",
	},
];

module.exports = { products };
