const swaggerConfig = {
	swagger: "2.0",
	info: {
		version: "1.0.0",
		title: "Express API",
		description: "An example Express app with Swagger documentation",
	},
	host: "localhost:3000",
	schemes: ["http"],
	paths: {
		"/signup": {
			post: {
				summary: "Create a new user",
				parameters: [
					{
						in: "body",
						name: "user",
						required: true,
						schema: {
							type: "object",
							properties: {
								email: {
									type: "string",
								},
								password: {
									type: "string",
								},
							},
						},
					},
				],
				responses: {
					201: {
						description: "User created",
					},
					409: {
						description:
							"A user with the specified email address already exists",
					},
				},
			},
		},
		"/login": {
			post: {
				summary: "Log in as an existing user",
				parameters: [
					{
						in: "body",
						name: "user",
						required: true,
						schema: {
							type: "object",
							properties: {
								email: {
									type: "string",
								},
								password: {
									type: "string",
								},
							},
						},
					},
				],
				responses: {
					200: {
						description: "Successful login",
						schema: {
							type: "object",
							properties: {
								email: {
									type: "string",
								},
							},
						},
					},
					401: {
						description: "Invalid email or password",
					},
				},
			},
		},
		"/cart": {
			get: {
				summary: "Get the list of products in the cart",
				parameters: [
					{
						in: "header",
						name: "Authorization",
						required: true,
						type: "string",
					},
				],
				responses: {
					200: {
						description: "Successful request",
						schema: {
							type: "array",
							items: {
								type: "object",
								properties: {
									id: {
										type: "integer",
									},
									name: {
										type: "string",
									},
									price: {
										type: "number",
									},
									quantity: {
										type: "integer",
									},
								},
							},
						},
					},
				},
			},
		},
	},
};

module.exports = swaggerConfig;
