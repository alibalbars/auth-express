const express = require("express");
const User = require("./models/user");
const UserStorage = require("./models/userStorage");
const jwt = require("jsonwebtoken");
const cors = require("cors");
const { verifyToken } = require("./middlewares");
const { products } = require("./models/products");
const cookieParser = require("cookie-parser");
require("dotenv").config();
const swaggerUi = require("swagger-ui-express");
const swaggerConfig = require("./models/swaggerConfig");

const app = express();

const port = process.env.PORT;
app.use(express.json());
app.use(
	cors({
		credentials: true,
		origin: (origin, callback) => callback(null, true),
	})
);
app.use(cookieParser());
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerConfig));

app.post("/signup", (req, res) => {
	let { email, password } = req.body;
	const userTryToSignUp = UserStorage.findUser({ email });

	if (userTryToSignUp) {
		res
			.status(409)
			.send("A user with the specified email address already exists.");
		return;
	}

	const newUser = new User({
		email,
		password,
	});

	UserStorage.addUser(newUser);

	res.status(201).send("User created.");
});

app.post("/login", (req, res) => {
	let { email, password } = req.body;
	const userTryToLogin = UserStorage.findUser({ email });
	if (!userTryToLogin) {
		res.status(401).send("Invalid email or password.");
		return;
	}

	if (password !== userTryToLogin.password) {
		res.status(401).send("Invalid email or password.");
		return;
	}
	// user email and password is correct
	const token = jwt.sign({ email: userTryToLogin }, process.env.SECRET_KEY);

	res.cookie("token", token, {
		httpOnly: false,
		secure: false,
		domain: process.env.COOKIE_DOMAIN,
	});
	res.status(200).send({ email });
});

app.get("/cart", verifyToken, (req, res) => {
	res.send(products);
});

app.listen(port, () => {
	console.log(`Listening on port ${port}`);
});
