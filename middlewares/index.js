const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
	let token;
	try {
		token = req.cookies.token;
		if (!token) {
			return res.status(403).send({
				message: "No token provided!",
			});
		}
	} catch {
		return res.status(403).send({
			message: "No token provided!",
		});
	}

	jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
		if (err) {
			return res.status(401).send({
				message: "Unauthorized!",
			});
		}
		req.email = decoded.email;
		next();
	});
};

module.exports = {
	verifyToken,
};
